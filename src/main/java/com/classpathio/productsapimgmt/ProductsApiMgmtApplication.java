package com.classpathio.productsapimgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsApiMgmtApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductsApiMgmtApplication.class, args);
    }

}
