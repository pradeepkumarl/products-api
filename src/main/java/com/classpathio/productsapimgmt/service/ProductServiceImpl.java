package com.classpathio.productsapimgmt.service;

import com.classpathio.productsapimgmt.model.Product;
import com.classpathio.productsapimgmt.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public Set<Product> fetchAll() {
        return new HashSet<>(this.productRepository.findAll());
    }

    @Override
    public Product fetchById(long productId) {
        return this.productRepository
                        .findById(productId)
                        .orElseThrow(() ->  new IllegalArgumentException("Invalid product id"));
    }

    @Override
    public void deleteProductById(long productId) {
        this.productRepository.deleteById(productId);
    }
}