package com.classpathio.productsapimgmt.service;

import com.classpathio.productsapimgmt.model.Product;

import java.util.Set;

public interface ProductService {

    Product save(Product product);

    Set<Product> fetchAll();

    Product fetchById(long productId);

    void deleteProductById(long productId);
}