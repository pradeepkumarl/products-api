package com.classpathio.productsapimgmt.controller;

import com.classpathio.productsapimgmt.model.Product;
import com.classpathio.productsapimgmt.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/v1/products")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public Set<Product> fetchProducts(){
        return this.productService.fetchAll();
    }

    @GetMapping("/{id}")
    public Product fetchById(@PathVariable("id") long productId){
        return this.productService.fetchById(productId);
    }

    @PostMapping
    public Product saveProduct(@RequestBody Product product){
        return this.productService.save(product);
    }

    @DeleteMapping("/{id}")
    public void deleteProductById(@PathVariable("id") long productId){
        this.productService.deleteProductById(productId);
    }

}