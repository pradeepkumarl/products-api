package com.classpathio.productsapimgmt.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of="productId")
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long productId;

    private String name;

    private double price;
}